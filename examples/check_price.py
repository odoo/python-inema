#!/usr/bin/env python3


# Example program for checking all the default product prices
# the package currently uses.
#
# Usecases:
#
# - verify that your personal products.json with your private discounts is properly integrated
# - generate a quick-fix products.json after yet another Deutsche Post price hike
#   is effective and you forgot to update it before
# - you didn't get the PPL csv announcement and now you want to quickly
#   generate an up-to-date products.json
#
# Example call:
#
#     $ ./check_price.py /path/to/your/config.ini products-$(date -I).json
#     $ diff -u /path/to/inema/data/product.json products-$(date -I).json
#
# NB: cf. update_products.py for generating the products.json from a PPL csv file


import configparser
from decimal import Decimal
import inema
import inema.inema
import json
import sys
import zeep




def main():
    ps = inema.inema.load_products()
    for i in ps:
        ps[i]['oprice']     = ps[i]['cost_price']
        ps[i]['cost_price'] = '0.01'

    filename = sys.argv[1]
    ofilename = sys.argv[2]

    conf = configparser.ConfigParser()
    conf.read(filename)
    im = inema.Internetmarke(conf['api']['id'], conf['api']['key'], conf['api'].get('key_phase', '1'),
                             ps)
    im.authenticate(conf['account']['user'], conf['account']['password'])

    d = conf['a.default']

    a = im.build_addr(d['street'], d['number'], d['zip'], d['city'], d['country'])
    ca = im.build_comp_addr(d['name'], a)

    #xs = [ '1', '195']
    xs = ps.keys()
    remove = []
    new_price = { }
    for x in xs:
        p = im.build_position(x, ca, ca, pdf=True, page=1, x=1, y=1);
        im.add_position(p)

        try:
            r = im.checkoutPDF(1)
        except zeep.exceptions.Fault as e:
          d = zeep.wsdl.utils.etree_to_string(e.detail).decode()
          ids = e.detail.xpath('//*[name()="id"]')
          ms = e.detail.xpath('//*[name()="message"]')
          d = " - ".join(", ".join(x.text for x in xs) for xs in (ids, ms))
          print(f'{e.message} --- {d}')
          if 'invalidProductcode' in d:
              remove.append(x)
          elif 'invalidTotalAmount' in d:
              ts = d.split()
              k = ts.index('PPL-total:')
              t = (Decimal(ts[k + 1])/100).quantize(Decimal('1.00'))
              if t != Decimal(ps[x]['oprice']):
                  new_price[x] = str(t)
          else:
              print('    unknown error ...')

        im.clear_positions()

    for x in remove:
        print(f"{x}: {ps[x]['name']} is removed")
        del ps[x]
    for x, y in new_price.items():
        print(f"{x}: {ps[x]['name']} price change: {ps[x]['oprice']} -> {y}")
        ps[x]['cost_price'] = y
        del ps[x]['oprice']

    for x in ps:
        if 'oprice' in ps[x]:
            ps[x]['cost_price'] = ps[x]['oprice']
            del ps[x]['oprice']

    json.dump(ps, open(ofilename, 'w'), sort_keys=True, indent=4, ensure_ascii=False)


if __name__ == '__main__':
    sys.exit(main())
